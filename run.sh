#!/bin/bash
echo "Script made by Isham Jaglan"
echo "Follow me on GitHub:"
echo "https://github.com/ixh4m"
mkdir downloaded-packages
cd downloaded-packages
sudo xbps-install -Syu axel void-repo-multilib void-repo-nonfree void-repo-multilib-nonfree flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
export PATH=$PATH:/var/lib/flatpak/exports/share:/home/$USER/.local/share/flatpak/exports/share
echo "export PATH=$PATH:/var/lib/flatpak/exports/share:/home/$USER/.local/share/flatpak/exports/share" >> ~/.bash_profile
echo "export PATH=$PATH:/var/lib/flatpak/exports/share:/home/$USER/.local/share/flatpak/exports/share" >> ~/.zshenv
source ~/.bash_profile
source ~/.zshenv
flatpak install -y flathub
sudo xbps-install -Syu
echo -e "\nWant to install Discord?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling Discord"
	    flatpak install -y com.discordapp.Discord
	    sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install LibreOffice?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling LibreOffice"
	    sudo xbps-install -y libreoffice
	    sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install VSCode?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling VSCode"
	    flatpak install -y com.visualstudio.code
            sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install Steam?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling Steam"
            sudo xbps-install -Sy steam libgcc-32bit libstdc++-32bit libdrm-32bit libglvnd-32bit mesa-dri-32bit mono
            sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install Hacking Tools?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\n So you want to be a hacker XD"
            echo -e "\nInstalling the tools"
            sudo xbps-install -Sy docker docker-cli nmap binwalk sqlmap wireshark ghidra john netcat hashcat thc-hydra ruby rust go sqlite nodejs python3 python3-pip wget gcc ruby-devel libxml2 libxml2-devel libxslt libxslt-devel inetutils
            sudo groupadd docker
            sudo usermod -aG docker $USER
            newgrp docker

            # rustscan
            docker pull rustscan/rustscan:latest
            echo "alias rustscan='docker run --rm -it  rustscan/rustscan'" >> ~/.bashrc
            source ~/.bashrc

            #wpscan
            docker pull wpscanteam/wpscan
            echo "alias wpsscan='docker run --rm -it  wpscanteam/wpscan'"
            
            #SecLists
            git clone https://github.com/danielmiessler/SecLists.git
            sudo mv SecLists /usr/share/
            
            #haiti-hash
            docker pull noraj/haiti
            echo "alias haiti='docker run --rm -it  noraj/haiti'" >> ~/.bashrc

            #ffuf
            sudo xbps-install -y ffuf

            #chisel
            docker pull jpillora/chisel
            echo "alias chisel='docker run --rm -it jpillora/chisel'" >> ~/.bashrc

            #nikto
            docker pull securecodebox/nikto
            echo "alias chisel='docker run --rm -it securecodebox/nikto'" >> ~/.bashrc

            #crackmapexec
            docker pull byt3bl33d3r/crackmapexec
            echo "alias crackmapexec='docker run --rm -it byt3bl33d3r/crackmapexec'" >> ~/.bashrc
            echo "alias cme='docker run --rm -it byt3bl33d3r/crackmapexec'" >> ~/.bashrc

            #metasploit framework
            docker pull metasploitframework/metasploit-framework
            echo "alias msfconsole='docker run --rm -it -p 443:443 -v ~/.msf4:/home/$USER/.msf4 -v /tmp/msf:/tmp/data metasploitframework/metasploit-framework'" >> ~/.bashrc
            echo -e "You can't hack you noob."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install Xtreme Download Manager?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling Xtreme Download Manager"
            axel "https://github.com/subhra74/xdm/releases/download/7.2.11/xdm-setup-7.2.11.tar.xz" -o "xdm.tar.xz"
	    tar -xf xdm.tar.xz
	    sudo ./install.sh
	    rm install.sh readme.sh xdm.tar.xz
	    sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install Spotify?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling Spotify"
	    flatpak install -y com.spotify.Client
	    sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done
echo -e "\nWant to install Bitwarden?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling Bitwarden"
	    flatpak install -y com.bitwarden.desktop
	    sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done

echo -e "\nWant to install QEMU?"
PS3="Please select an option : "
choices=("yes" "no")
select choice in "${choices[@]}"; do
        case $choice in
          yes)

            echo -e "\nInstalling QEMU and Virt-Manager"
            sudo xbps-install -y qemu virt-manager

            sleep 1
            break
            ;;
          no)
            echo -e "Nise."
            sleep 1
            break
            ;;
  esac
done

cd .. && rm -rf downloaded-packages
echo -e "\n\n\nScript made by Isham Jaglan"
echo "Follow me on GitHub:"
echo "https://github.com/ixh4m"
firefox https://github.com/ixh4m
